package com.carolynvs.stash.plugin.force_field;

import java.util.Map;
import java.util.HashMap;
import java.io.IOException;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.nav.NavBuilder;

import com.atlassian.stash.hook.repository.RepositoryHookService;

/**
 * @author Jim Bethancourt
 */
public class ForceFieldConfigServlet extends HttpServlet {

    static final String SETTINGS_MAP = "com.carolynvs.force-field.settings";

    private final RepositoryHookService repositoryHookService;
    private static final Logger log = LoggerFactory.getLogger(ForceFieldConfigServlet.class);
    final private SoyTemplateRenderer soyTemplateRenderer;
    private final NavBuilder navBuilder;
    private Map<String, String> fields;
    private Map<String, Iterable<String>> fieldErrors;
    private final PluginSettings pluginSettings;
    private HashMap<String, Object> settingsMap;

    public ForceFieldConfigServlet(SoyTemplateRenderer soyTemplateRenderer,
                                   PluginSettingsFactory pluginSettingsFactory,
                                   RepositoryHookService repositoryHookService,
                                   NavBuilder navBuilder) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.navBuilder = navBuilder;
        this.repositoryHookService = repositoryHookService;

        pluginSettings = pluginSettingsFactory.createGlobalSettings();

        fields = new HashMap<String, String>();
        fieldErrors = new HashMap<String, Iterable<String>>();
    }


    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.debug("doGet");
        settingsMap = (HashMap<String, Object>) pluginSettings.get(SETTINGS_MAP);
        if (settingsMap == null) {
            settingsMap = new HashMap<String, Object>();
        }

        doGetContinue(req, resp);
    }

    protected void doGetContinue(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.debug("doGetContinue");
        fields.clear();

        for (Map.Entry<String, Object> entry : settingsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            log.debug("got plugin config " + key + "=" + value + " " + value.getClass().getName());
            if (value instanceof String) {
                fields.put(key, (String) value);
            }
        }

        log.debug("Config fields: " + fields);
        log.debug("Field errors: " + fieldErrors);

        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), "com.carolynvs.force-field:forceField-config-serverside", "com.carolynvs.stash.plugin.force_field.config",
                    ImmutableMap
                            .<String, Object>builder()
                            .put("config", fields)
                            .put("errors", fieldErrors)
                            .build()
            );
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }

    }

    void addStringFieldValue(Map<String, Object> settingsMap, HttpServletRequest req, String fieldName) {
        String o;
        o = req.getParameter(fieldName);
        if (o != null && !o.isEmpty()) settingsMap.put(fieldName, o);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        settingsMap.clear();

        // Plugin globalConfig persister supports only map of strings
        for (Object key : req.getParameterMap().keySet()) {
            String parameterName = (String) key;

            // Plugin settings persister only supports map of strings
            if (!parameterName.startsWith("errorMessage") && !parameterName.equals("submit")) {
                addStringFieldValue(settingsMap, req, parameterName);
            }
        }

        if (fieldErrors.size() > 0) {
            doGetContinue(req, resp);
            return;
        }

        if(log.isDebugEnabled()) {
            for (Map.Entry<String, Object> entry : settingsMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                log.debug("save plugin config " + key + "=" + value + " " + value.getClass().getName());
            }
        }

        pluginSettings.put(SETTINGS_MAP, settingsMap);

        String redirectUrl;
        redirectUrl = navBuilder.addons().buildRelative();
        log.debug("redirect: " + redirectUrl);
        resp.sendRedirect(redirectUrl);
    }
}

package com.carolynvs.stash.plugin.force_field;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.hook.repository.RepositoryHookService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsBuilder;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Jim Bethancourt
 */
public class ForceFieldConfigServletTest {

    @Mock private SoyTemplateRenderer soyTemplateRenderer;
    @Mock private PluginSettingsFactory pluginSettingsFactory;
    @Mock private PluginSettings pluginSettings;
    @Mock private RepositoryHookService repositoryHookService;
    @Mock private NavBuilder navBuilder;
    @Mock private NavBuilder.Addons addons;
    @Mock private SettingsBuilder settingsBuilder;
    @Mock private Settings settings;

    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;

    ForceFieldConfigServlet forceFieldConfigServlet;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
        when(pluginSettings.get(anyString())).thenReturn(new HashMap<String, String>());

        when(repositoryHookService.createSettingsBuilder()).thenReturn(settingsBuilder);
        when(settingsBuilder.build()).thenReturn(settings);
        when(settingsBuilder.addAll(anyMap())).thenReturn(settingsBuilder);

        when(navBuilder.addons()).thenReturn(addons);
        when(addons.buildRelative()).thenReturn("/forceField/config");

        forceFieldConfigServlet = new ForceFieldConfigServlet(soyTemplateRenderer,
                pluginSettingsFactory, repositoryHookService, navBuilder);
    }

    @Test
    public void testDoGet() throws IOException, ServletException, SoyException {
        forceFieldConfigServlet.doGet(request, response);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("config", new HashMap());
        map.put("errors", new HashMap());
        verify(soyTemplateRenderer, times(1)).render(null,
                "com.carolynvs.force-field:forceField-config-serverside", "com.carolynvs.stash.plugin.force_field.config", map);
    }

    @Test
    public void testDoPostNoParams() throws IOException, ServletException {
        forceFieldConfigServlet.doGet(request, response); // calling doGet to populate the settings map
        forceFieldConfigServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect("/forceField/config");
    }

    @Test
    public void testDoPostWithParams() throws IOException, ServletException {
        Map<String, Object> parameterMap = new HashMap<String, Object>();

        parameterMap.put("references", "master");

        when(pluginSettings.get(anyString())).thenReturn(parameterMap);

        forceFieldConfigServlet = new ForceFieldConfigServlet(soyTemplateRenderer,
                pluginSettingsFactory, repositoryHookService, navBuilder);

        when(request.getParameterMap()).thenReturn(parameterMap);

        forceFieldConfigServlet.doGet(request, response); // calling doGet to populate the settings map
        forceFieldConfigServlet.doPost(request, response);

        spy(forceFieldConfigServlet).addStringFieldValue(parameterMap, request, "references");
        verify(response, times(1)).sendRedirect("/forceField/config");
    }

}

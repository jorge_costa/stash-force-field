Force Field
========================

[Download the latest version from Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.carolynvs.force-field)

This is a pre-receive hook plugin for Atlassian Stash. It protects specific branches from force pushes, e.g git push --force. Simply enter the list of branches to protect, separated by spaces. You may use Ant regex to protect all branches which match a pattern.

For example: `master release/**` would protect master and all branches which start with release, such as release/v1/1.0, release/v2/2.1, etc.

